const gulp = require('gulp');
const concat = require('gulp-concat');
const plumber = require('gulp-plumber');
const minifyjs = require('gulp-js-minify');
const rename = require('gulp-rename');



module.exports = function js() {
  return gulp.src('src/scripts/*.js')
    .pipe(plumber())
    .pipe(concat('scripts.js'))
    .pipe(minifyjs())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('dist/scripts'))
}
