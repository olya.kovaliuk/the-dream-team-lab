const gulp = require('gulp');
const htmlValidator = require('gulp-w3c-html-validator');
const plumber = require('gulp-plumber');


module.exports = function html() {
  return gulp.src('*.html')
    .pipe(plumber())
    .pipe(htmlValidator())
    .pipe(gulp.dest('dist'))
}
